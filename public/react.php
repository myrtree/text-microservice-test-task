<?php

use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Psr\Http\Message\ServerRequestInterface;

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| First we need to get an application instance. This creates an instance
| of the application / container and bootstraps the application so it
| is ready to receive HTTP / Console requests from the environment.
|
*/

$app = require __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$loop = React\EventLoop\Factory::create();
$httpFoundationFactory = new HttpFoundationFactory();
$server = new React\Http\Server(
    function (ServerRequestInterface $reactRequest) use (
        $app,
        $httpFoundationFactory
    ) {
        $symfonyRequest = $httpFoundationFactory->createRequest($reactRequest);
        $response = $app->handle($symfonyRequest);

        return new React\Http\Response(
            $response->getStatusCode(),
            $response->headers->all(),
            $response->getContent()
        );
    }
);

$socket = new React\Socket\Server(env('LISTEN_ON'), $loop);
$server->listen($socket);
$loop->run();

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/6/2018
 * Time: 6:49 PM
 */


class CheckTextProcessorsTest extends TestCase
{
    public function testStripTags()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => '<b>some</b><br> <i>text</i>',
                'methods' => ['stripTags']
            ]
        ])->seeJsonEquals([
            'text' => 'some text'
        ]);
    }

    public function testRemoveSpaces()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => ' some    text ',
                'methods' => ['removeSpaces']
            ]
        ])->seeJsonEquals([
            'text' => 'sometext'
        ]);
    }

    public function testTrim()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => ' 	some text ',
                'methods' => ['trim']
            ]
        ])->seeJsonEquals([
            'text' => 'some text'
        ]);
    }

    public function testReplaceSpacesToEol()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => 'some short text',
                'methods' => ['replaceSpacesToEol']
            ]
        ])->seeJsonEquals([
            'text' => "some\nshort\ntext"
        ]);
    }

    public function testHtmlspecialchars()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => '<a href="test">Test</a>',
                'methods' => ['htmlspecialchars']
            ]
        ])->seeJsonEquals([
            'text' => '&lt;a href=&quot;test&quot;&gt;Test&lt;/a&gt;'
        ]);
    }

    public function testRemoveSymbols()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => '$ome#%text!',
                'methods' => ['removeSymbols']
            ]
        ])->seeJsonEquals([
            'text' => 'ometext'
        ]);
    }

    public function testToNumber()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => 'some42text 500',
                'methods' => ['toNumber']
            ]
        ])->seeJsonEquals([
            'text' => '42'
        ]);
    }

    public function testSeveralProcessorsTogether()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => 'oh 12<br>  <b>34</b> look',
                'methods' => ['stripTags' ,'removeSpaces', 'toNumber']
            ]
        ])->seeJsonEquals([
            'text' => '1234'
        ]);
    }
}

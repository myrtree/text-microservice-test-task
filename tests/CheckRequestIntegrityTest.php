<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/6/2018
 * Time: 6:49 PM
 */


class CheckRequestIntegrityTest extends TestCase
{
    public function testIfThereIsNoMethod()
    {
        $this->json('POST', '/', [
            'job' => [
                'text' => 'some text'
            ]
        ])->seeJsonEquals([
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\BadRequestHttpException',
            'message' => 'The methods field is required.'
        ]);
    }

    public function testIfThereIsNoText()
    {
        $this->json('POST', '/', [
            'job' => [
                'methods' => ['stripTags']
            ]
        ])->seeJsonEquals([
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\BadRequestHttpException',
            'message' => 'The text field is required.'
        ]);
    }

    public function testIfNothingProvided()
    {
        $this->json('POST', '/', [
            'job' => [
            ]
        ])->seeJsonEquals([
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\BadRequestHttpException',
            'message' => 'The text field is required.The methods field is required.'
        ]);

        $this->json('POST', '/', [
        ])->seeJsonEquals([
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\BadRequestHttpException',
            'message' => 'The text field is required.The methods field is required.'
        ]);
    }

    public function testIfAllRight()
    {
        $this->json('POST', '/', [
            'job' =>[
                'text' => '<b>some</b> text',
                'methods' => ['stripTags']
            ]
        ])->seeJson([
            'text' => 'some text'
        ]);
    }
}

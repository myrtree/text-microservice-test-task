<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 3:30 PM
 */

namespace App\TextProcessors;


class HtmlSpecialChars implements TextProcessorInterface
{
    public function process(string $input): string
    {
        return htmlspecialchars($input);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 3:55 PM
 */

namespace App\TextProcessors;


class ToNumber implements TextProcessorInterface
{
    public function process(string $input): string
    {
        preg_match('~(\d+)~', $input, $matches);

        return $matches[1];
    }
}

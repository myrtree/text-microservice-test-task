<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 3:28 PM
 */

namespace App\TextProcessors;


class ReplaceSpacesToEol implements TextProcessorInterface
{
    public function process(string $input): string
    {
        return preg_replace('~\s~', "\n", $input);
    }
}

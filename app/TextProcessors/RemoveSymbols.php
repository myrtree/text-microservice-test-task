<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 3:40 PM
 */

namespace App\TextProcessors;


class RemoveSymbols implements TextProcessorInterface
{
    public function process(string $input): string
    {
        return preg_replace('~[\[\.,\/!@#\$%&\*()\]]~', '', $input);
    }
}

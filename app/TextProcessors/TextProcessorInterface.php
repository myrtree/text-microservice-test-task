<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 1:59 PM
 */

namespace App\TextProcessors;

interface TextProcessorInterface
{
    public function process(string $input): string;
}
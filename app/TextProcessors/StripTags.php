<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/5/2018
 * Time: 2:03 PM
 */

namespace App\TextProcessors;


class StripTags implements TextProcessorInterface
{
    public function process(string $input): string
    {
        return strip_tags($input);
    }
}
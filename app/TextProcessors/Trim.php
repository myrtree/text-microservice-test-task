<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/7/2018
 * Time: 1:01 AM
 */

namespace App\TextProcessors;


class Trim implements TextProcessorInterface
{
    public function process(string $input): string
    {
        return trim($input);
    }
}

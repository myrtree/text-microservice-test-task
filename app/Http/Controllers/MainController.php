<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MainController extends Controller
{
    public function __invoke(Request $request)
    {
        $job = $request->json()->get('job') ?? [];

        $validator = Validator::make($job, [
            'text' => 'required|string',
            'methods' => 'required|array',
            'methods.*' => 'string'
        ]);

        if ($validator->fails()) {
            throw new BadRequestHttpException(
                array_reduce($validator->errors()->all(), function ($c, $i) {return $c.$i;})
            );
        }

        $text = $job['text'];
        foreach ($job['methods'] as $method) {
            $processor = "App\\TextProcessors\\{$method}";
            if (!class_exists($processor, false)) {
                app()->singleton($processor);
            }

            try {
                $text = (app()->make($processor))->process($text);
            } catch (\ReflectionException $e) {
                throw new BadRequestHttpException("method '{$method}' not found");
            }
        }

        return ['text' => $text];
    }
}
